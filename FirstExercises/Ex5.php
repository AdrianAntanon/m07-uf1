<?php

$longNumber = 10002345;
$result = calculateNumberOfDigits($longNumber);

echo "El número $longNumber tiene un total de $result dígitos";

function calculateNumberOfDigits($number){
    $div = 10;
    $count = 0;

    do{
        $number = floor( $number/$div);
        $count++;
    }while($number > 0);
    
    return $count;
}
