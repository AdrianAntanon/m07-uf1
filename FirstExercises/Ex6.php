<?php

$word = "somos";

checkPalindrome($word);

function checkPalindrome($originalWord)
{
    $reverseWord = strrev($originalWord);

    if (strcmp($originalWord, $reverseWord) === 0) {
        echo "La cadena $originalWord es un palíndromo";
    } else {
        echo "La cadena $originalWord no es un palíndromo";
    }
}

