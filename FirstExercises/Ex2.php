<?php

$number = 100;
echo "La lista de números primos del 1 al 100 es: <br>";


for ($i=1; $i<=$number; $i++) {

    if (primeNumber($i)) {

        echo "$i, ";

    }

}


function primeNumber($num){

    if ($num == 2 || $num == 3 || $num == 5 || $num == 7) {

        return True;

    } else {
        if ($num % 2 != 0) {
            for ($i = 3; $i <= sqrt($num); $i += 2) {
                if ($num % $i == 0) {
                    return False;
                }
            }
            return True;
        }
    }

    return False;

}
